package Model;

import java.sql.Date;

/**
 *
 * @author marco
 */
public class Pecas {

    public Pecas(int id, String idFornecedor, String nome, String mar, String mod, String quant, String valComp, String valRev, String desconto, Date dtCadastro, String codigoPeca, String descricao, int status) {
        this.id = id;
        this.idFornecedor = idFornecedor;
        this.nome = nome;
        this.mar = mar;
        this.mod = mod;
        this.quant = quant;
        this.valComp = valComp;
        this.valRev = valRev;
        this.desconto = desconto;
        this.dtCadastro = dtCadastro;
        this.codigoPeca = codigoPeca;
        this.descricao = descricao;
        this.status = status;
    }

    public Pecas() {
    }

    private int id;
    private String idFornecedor;
    private String nome;
    private String mar;
    private String mod;
    private String quant;
    private String valComp;
    private String valRev;
    private String desconto;
    private Date dtCadastro;
    private String codigoPeca;
    private String descricao;
    private int status;

    /**
     * @return the idFornecedor
     */

    public String getIdFornecedor() {
        return idFornecedor;
    }

    /**
     * @param idFornecedor the idFornecedor to set
     */
    public void setIdFornecedor(String idFornecedor) {
        this.idFornecedor = idFornecedor;
    }

    /**
     * @return the CodigoPeca
     */
    public String getCodigoPeca() {
        return codigoPeca;
    }

    /**
     * @param CodigoPeca the CodigoPeca to set
     */
    public void setCodigoPeca(String CodigoPeca) {
        this.codigoPeca = CodigoPeca;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the mar
     */
    public String getMar() {
        return mar;
    }

    /**
     * @param mar the mar to set
     */
    public void setMar(String mar) {
        this.mar = mar;
    }

    /**
     * @return the mod
     */
    public String getMod() {
        return mod;
    }

    /**
     * @param mod the mod to set
     */
    public void setMod(String mod) {
        this.mod = mod;
    }

    /**
     * @return the quant
     */
    public String getQuant() {
        return quant;
    }

    /**
     * @param quant the quant to set
     */
    public void setQuant(String quant) {
        this.quant = quant;
    }

    /**
     * @return the valComp
     */
    public String getValComp() {
        return valComp;
    }

    /**
     * @param valComp the valComp to set
     */
    public void setValComp(String valComp) {
        this.valComp = valComp;
    }

    /**
     * @return the valRev
     */
    public String getValRev() {
        return valRev;
    }

    /**
     * @param valRev the valRev to set
     */
    public void setValRev(String valRev) {
        this.valRev = valRev;
    }

    /**
     * @return the desc
     */
    public String getDesconto() {
        return desconto;
    }

    /**
     * @param desconto the desc to set
     */
    public void setDesconto(String desconto) {
        this.desconto = desconto;
    }

    /**
     * @return the dtCadastro
     */
    public Date getDtCadastro() {
        return dtCadastro;
    }

    /**
     * @param dtCadastro the dtCadastro to set
     */
    public void setDtCadastro(Date dtCadastro) {
        this.dtCadastro = dtCadastro;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

}
