/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package conect;

import Model.Fornecedores;
import Model.Pecas;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author marco
 */
public class PesquisaDAO {

    Fornecedores objFor = new Fornecedores();
    Pecas objPec = new Pecas();

    public boolean PesquisarForn(Fornecedores objFor) throws SQLException {
        int cod = objFor.getFor_id();

        Conexao conexao = new Conexao();
        PreparedStatement pstmt = conexao.getConexao().prepareStatement("SELECT * FROM fornecedor where for_id = ?");
        pstmt.setInt(1, cod);

        ResultSet rs = pstmt.executeQuery();

        if (rs.next()) {
            objFor.setRazaoSocial(rs.getString("razaosocial"));
            objFor.setCnpj(rs.getString("cnpj"));
            objFor.setEmail(rs.getString("email"));
            objFor.setTel(rs.getString("tel"));
            objFor.setRua(rs.getString("rua"));
            objFor.setNumero(rs.getString("numero"));
            objFor.setBairro(rs.getString("bairro"));
            objFor.setCep(rs.getString("cep"));
            objFor.setCidade(rs.getString("cidade"));
            objFor.setEstado(rs.getString("estado"));
            objFor.setDtCadastro(rs.getDate("dtcadastro"));
            objFor.setStatus(rs.getInt("status"));
        }

        return true;
    }///End PesquisarForn

    public boolean PesquisarPec(Pecas objPec) throws SQLException {
        int cod = objFor.getFor_id();

        Conexao conexao = new Conexao();
        PreparedStatement pstmt = conexao.getConexao().prepareStatement("SELECT * FROM pecas where pec_id = ?");
        pstmt.setInt(1, cod);

        ResultSet rs = pstmt.executeQuery();

        if (rs.next()) {
            objPec.setNome(rs.getString("pec_nome"));
            objPec.setMar(rs.getString("pec_mar"));
            objPec.setMod(rs.getString("pec_mod"));
            objPec.setQuant(rs.getString("pec_quant"));
            objPec.setValComp(rs.getString("pec_valcomp"));
            objPec.setValRev(rs.getString("pec_valrev"));
            objPec.setDesconto(rs.getString("pec_desc"));
            objPec.setDtCadastro(rs.getDate("pec_dtcadastro"));
        }
        return true;
    }///End PesquisarPec
}
