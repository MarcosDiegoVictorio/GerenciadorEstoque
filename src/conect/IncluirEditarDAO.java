/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package conect;

import Model.Fornecedores;
import Model.Pecas;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author marco
 */
public class IncluirEditarDAO {

    Fornecedores objFor = new Fornecedores();
    Pecas objPec = new Pecas();

    public int IncluirFor(Fornecedores objFor) throws SQLException {
        Conexao conexao = new Conexao();
        int registro;

        String query = "select * from db_estoque.fornecedores where cnpj = ?";
        try ( PreparedStatement consult = conexao.getConexao().prepareStatement(query)) {
            consult.setString(1, objFor.getCnpj().trim());

            ResultSet cnpj = consult.executeQuery();

            if (cnpj.next() == true) {
                JOptionPane.showMessageDialog(null, "CNPJ existente, operação cancelada!");
            } else {
                query = "INSERT INTO db_estoque.fornecedores (razaosocial, cnpj, email, tel, rua, numero, bairro, cep, cidade, estado, dtcadastro, status) VALUES (?,?,?,?,?,?,?,?,?,?,sysdate(),?)";
                try ( PreparedStatement pstmt = conexao.getConexao().prepareStatement(query)) {
                    pstmt.setString(1, objFor.getRazaoSocial().toUpperCase().trim());
                    pstmt.setString(2, objFor.getCnpj().trim());
                    pstmt.setString(3, objFor.getEmail().trim());
                    pstmt.setString(4, objFor.getTel().trim());
                    pstmt.setString(5, objFor.getRua().trim());
                    pstmt.setString(6, objFor.getNumero().trim());
                    pstmt.setString(7, objFor.getBairro().trim());
                    pstmt.setString(8, objFor.getCep().trim());
                    pstmt.setString(9, objFor.getCidade().trim());
                    pstmt.setString(10, objFor.getEstado().trim());
                    pstmt.setInt(11, objFor.getStatus());

                    registro = pstmt.executeUpdate();

                    PreparedStatement pstmAux = conexao.getConexao().prepareStatement("SELECT @@IDENTITY");
                    ResultSet rs = pstmAux.executeQuery();
                    int codigo = 0;

                    if ((registro == 1) && (rs != null)) {
                        rs.next();
                        codigo = rs.getInt(1);
                        return codigo;
                    } else {
                        return codigo;
                    }
                }
            }
        }
        return 0;
    } ///end IncluirFor

    public boolean EditarFor(Fornecedores objFor) throws SQLException {
        Conexao conexao = new Conexao();
        int registro;

        String query = "UPDATE db_estoque.fornecedores SET razaosocial=?,cnpj=?,email=?,tel=?,rua=?,numero=?,bairro=?,cep=?,cidade=?,estado=?,dtcadastro=sysdate(),status=? where (id = ?)";
        try ( PreparedStatement pstmt = conexao.getConexao().prepareStatement(query)) {
            pstmt.setString(1, objFor.getRazaoSocial().toUpperCase().trim());
            pstmt.setString(2, objFor.getCnpj().trim());
            pstmt.setString(3, objFor.getEmail().trim());
            pstmt.setString(4, objFor.getTel().trim());
            pstmt.setString(5, objFor.getRua().trim());
            pstmt.setString(6, objFor.getNumero().trim());
            pstmt.setString(7, objFor.getBairro().trim());
            pstmt.setString(8, objFor.getCep().trim());
            pstmt.setString(9, objFor.getCidade().trim());
            pstmt.setString(10, objFor.getEstado().trim());
            pstmt.setInt(11, objFor.getStatus());
            pstmt.setInt(12, objFor.getId());

            registro = pstmt.executeUpdate();
        }
        if (registro == 1) {
            return true;
        } else {
            return false;
        }
    }///end EditarFor

    public int IncluirPec(Pecas objPec) throws SQLException {
        Conexao conexao = new Conexao();
        int registro;

        String query = "select * from db_estoque.pecas where codigo = ?";
        try ( PreparedStatement consult = conexao.getConexao().prepareStatement(query)) {
            consult.setString(1, objPec.getCodigoPeca().trim());

            ResultSet codigo = consult.executeQuery();

            query = "INSERT INTO db_estoque.pecas (codigo, nome, marca, modelo, quantidade, valorcompra, valorrevenda, desconto, descricao, dtcadastro) VALUES (?,?,?,?,?,?,?,?,?,sysdate())";
            try ( PreparedStatement pstmt = conexao.getConexao().prepareStatement(query)) {
                pstmt.setString(1, objPec.getCodigoPeca().trim());
                pstmt.setString(2, objPec.getNome().trim());
                pstmt.setString(3, objPec.getMod().trim());
                pstmt.setString(4, objPec.getMar().trim());
                pstmt.setString(5, objPec.getQuant().trim());
                pstmt.setString(6, objPec.getValComp().trim());
                pstmt.setString(7, objPec.getValRev().trim());
                pstmt.setString(8, objPec.getDesconto().trim());
                pstmt.setString(9, objPec.getDescricao().trim());

                registro = pstmt.executeUpdate();

                PreparedStatement pstmAux = conexao.getConexao().prepareStatement("SELECT @@IDENTITY");
                ResultSet rs = pstmAux.executeQuery();
                int codi = 0;

                if ((registro == 1) && (rs != null)) {
                    rs.next();
                    codi = rs.getInt(1);
                    return codi;
                } else {
                    return codi;
                }
            }

        }

    }///End IncluirPec

    public boolean EditarPec(Pecas objPec) throws SQLException {
        Conexao conexao = new Conexao();
        int registro;

        String query = "UPDATE db_estoque.pecas SET idfornecedor=?, codigo=?, nome=?, marca=?, modelo=?, quantidade=?, valorcompra=?, valorrevenda=?, desconto=?, descricao=?, dtcadastro=sysdate() where id = ?";
        try ( PreparedStatement pstmt = conexao.getConexao().prepareStatement(query)) {

            String teste = objPec.getIdFornecedor();
            System.out.println(teste);
            pstmt.setString(1, objPec.getIdFornecedor().trim());
            pstmt.setString(2, objPec.getCodigoPeca().trim());
            pstmt.setString(3, objPec.getNome().toUpperCase().trim());
            pstmt.setString(4, objPec.getMar().trim());
            pstmt.setString(5, objPec.getMod().trim());
            pstmt.setString(6, objPec.getQuant().trim());
            pstmt.setString(7, objPec.getValComp().trim());
            pstmt.setString(8, objPec.getValRev().trim());
            pstmt.setString(9, objPec.getDesconto().trim());
            pstmt.setString(10, objPec.getDescricao().trim());
            pstmt.setInt(11, objPec.getId());

            registro = pstmt.executeUpdate();
        }
        if (registro == 1) {
            return true;
        } else {
            return false;
        }
    }

}///end class incluirEditar
