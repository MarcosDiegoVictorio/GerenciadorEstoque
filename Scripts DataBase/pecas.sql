create table pecas (
	id int NOT NULL primary key auto_increment,
    idfornecedor varchar(300),
    codigo varchar(300),
    nome varchar(300), 
    marca varchar(300), 
    modelo varchar(300),
    quantidade varchar(300), 
    valorcompra varchar(300), 
    valorrevenda varchar(300), 
    desconto varchar(300), 
    descricao varchar(1000), 
    dtcadastro datetime,
	FOREIGN KEY (id) REFERENCES pecas(idfornecedor)
)ENGINE=MyISAM auto_increment=17 default charset=utf8 row_format=compressed;