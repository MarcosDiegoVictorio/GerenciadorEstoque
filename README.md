### Sistema Gerenciador de Estoque

#### Sistema para gerenciamento de estoque.

###### Funcionalidades do Sistema: Adicionar, Editar e excluir Fornecedore e Peças.

### Tecnologias utilizadas:

###### Database: Mysql server 8.0.21.

###### Linguagem: Java.

###### IDE de desenvolvimento utilizada: Netbeans.

<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/mysql/mysql-original-wordmark.svg" width="60" height="60"/> <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/java/java-original-wordmark.svg" width="60" height="60"/> <img src="https://upload.wikimedia.org/wikipedia/commons/9/98/Apache_NetBeans_Logo.svg" width="60" height="60"/>

#### Primeiros passos para iniciar o Sistema pela primeira vez.

##### Criação da Base de dados:

###### 1° - Abra a pasta "Scripts DataBase", que encontra-se dentro da pasta do projeto, ela contém os arquivos com os scripts sql para criação da base de dados:

###### "Database.sql" : para criação da base de dados.

###### "fornecedores.sql" : para criação da tabela de fornecedores.

###### "pecas.sql" : para criação da tabela de peças.

###### 2° - Usuario senha e ip, do servidor utilizado no desenvolvomento e conexão do banco de dados.

###### Usuario: diego

###### Senha: Md.12345678910

###### ip: //127.0.0.1:3306 "Padrão do banco mysql".

###### 3° - Dentro da Pasta Dist está o arquivo "GerenciadorEstoque.jar" para a execussão do sistema
